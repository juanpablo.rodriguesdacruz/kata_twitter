import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    application
    kotlin("plugin.serialization") version "1.7.10"
    kotlin("jvm") version "1.7.10"
}

group = "me.user"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    //Ktor Server
    implementation ("io.ktor:ktor-server-core:2.0.3")
    implementation ("io.ktor:ktor-server-netty:2.0.3")
    implementation ("io.ktor:ktor-server-content-negotiation:2.0.3")
    implementation ("io.ktor:ktor-serialization-kotlinx-json:2.0.3")
    implementation ("io.ktor:ktor-server-default-headers:2.0.3")
    implementation ("io.ktor:ktor-server-cors:2.0.3")
    implementation ("io.ktor:ktor-server-call-logging:2.0.3")
    implementation ("io.ktor:ktor-server-compression:2.0.3")
    implementation ("io.ktor:ktor-server-status-pages:2.0.3")
    implementation ("io.ktor:ktor-server-forwarded-header:2.0.3")

    //Ktor Client
    implementation ("io.ktor:ktor-client-core:2.0.3")
    implementation ("io.ktor:ktor-client-apache:2.0.3")
    implementation ("io.ktor:ktor-client-content-negotiation:2.0.3")

    testImplementation(kotlin("test"))
    testImplementation ("io.mockk:mockk:1.12.3")
}

tasks.test {
    useJUnit()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

application {
    mainClass.set("MainKt")
}