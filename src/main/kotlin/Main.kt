import core.action.*
import core.domain.UserService
import core.http.Ktor
import core.infrastructure.InMemoryUserRepository
import java.io.File
import java.io.InputStream
import java.io.PrintWriter

fun main(args: Array<String>) {
    val userRepository = InMemoryUserRepository()
    val userService = UserService(userRepository)
    val followingUser = FollowingUser(userRepository)
    val registerUser = RegisterUser(userService)
    val followUser = FollowUser(userRepository)
    val updateUser = UpdateUser(userService)
    val findUser = FindUser(userRepository)
    val createTwit = CreateTwit(userRepository)
    val ktor = Ktor(followingUser, registerUser, followUser, updateUser, findUser, createTwit)
    ktor.startServer()
}