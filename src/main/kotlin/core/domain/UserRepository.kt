package core.domain

interface UserRepository {
    fun save(userToRegister: User)
    fun update(nickNameToUpdate: User)
    fun find(nickName: String): User?
}
