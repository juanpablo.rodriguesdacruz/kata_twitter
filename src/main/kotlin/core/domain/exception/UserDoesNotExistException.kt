package core.domain.exception

class UserDoesNotExistException : Exception()
