package core.domain

data class User(
    val realName: String,
    val nickName: String
) {
    private var mFollowers: MutableSet<String> = mutableSetOf()
    val followers: Set<String>
        get() = mFollowers.toSet()

    fun follow(nickName: String) {
        mFollowers.add(nickName)
    }

    private val mTwit: MutableList<String> = mutableListOf()
    val twits: List<String>
        get() = mTwit

    fun addTwit(text: String) {
        mTwit.add(text)
    }
}
