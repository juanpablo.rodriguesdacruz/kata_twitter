package core.domain

import core.domain.exception.UserExistsException
import core.domain.exception.UserDoesNotExistException

class UserService(private val repository: UserRepository) {
    fun create(user: User) {
        val userToSave: User? = repository.find(user.nickName)
        if (userToSave != null)
            throw UserExistsException()
        repository.save(user)
    }

    fun update(user: User) {
        checkUserExists(user)
        repository.update(user)
    }

    private fun checkUserExists(user: User) = repository.find(user.nickName) ?: throw UserDoesNotExistException()
}
