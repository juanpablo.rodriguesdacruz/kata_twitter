package core.infrastructure

import core.domain.User
import core.domain.UserRepository

class InMemoryUserRepository: UserRepository {
    private val users = mutableMapOf<String, User>()

    override fun save(userToRegister: User) {
        users[userToRegister.nickName] = userToRegister
    }

    override fun update(nickNameToUpdate: User) {
        if (find(nickNameToUpdate.nickName) != null) {
            users[nickNameToUpdate.nickName] = nickNameToUpdate
        }
    }

    override fun find(nickName: String): User? {
        return users[nickName]
    }
}
