package core.action

import core.domain.User
import core.domain.UserService

class UpdateUser(private val userService: UserService) {
    operator fun invoke(actionData: ActionData) {
        val user = User(actionData.realName, actionData.nickName)
        userService.update(user)
    }

    data class ActionData(
        val realName : String,
        val nickName : String
    )
}
