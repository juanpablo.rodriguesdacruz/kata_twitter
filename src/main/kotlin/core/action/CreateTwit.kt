package core.action

import core.domain.User
import core.domain.UserRepository
import core.domain.exception.UserDoesNotExistException

class CreateTwit (private val repository: UserRepository) {
    operator fun invoke(actionData: ActionData): User {
        val user = checkFollowerExists(actionData)
        return user.let { user ->
            user.addTwit(actionData.twit)
            repository.save(user)
            user
        }
    }

    private fun checkFollowerExists(actionData: ActionData) =
        repository.find(actionData.nickName) ?: throw UserDoesNotExistException()

    data class ActionData(
        val nickName : String,
        val twit: String
    )
}
