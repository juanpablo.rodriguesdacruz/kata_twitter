package core.action

import core.domain.User
import core.domain.UserRepository
import core.domain.exception.UserDoesNotExistException

class FollowUser(private val userRepository: UserRepository) {
    operator fun invoke(actionData: ActionData): User {
        val followed = checkUserExists(actionData.followed)
        val follower = checkUserExists(actionData.follower)

        return follower.let { user ->
            user.follow(followed.nickName)
            userRepository.save(user)
            user
        }
    }

    private fun checkUserExists(user: String) =
        userRepository.find(user) ?: throw UserDoesNotExistException()

    data class ActionData(val follower: String, val followed: String)
}
