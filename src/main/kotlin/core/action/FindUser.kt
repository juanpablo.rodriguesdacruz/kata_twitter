package core.action

import core.domain.User
import core.domain.UserRepository
import core.domain.exception.UserDoesNotExistException

class FindUser(private val repository: UserRepository) {
    operator fun invoke(actionData: ActionData): User {
        return findUser(actionData) ?: throw UserDoesNotExistException()
    }

    private fun findUser(actionData: ActionData) = repository.find(actionData.nickName)

    data class ActionData(
        val nickName : String
    )
}
