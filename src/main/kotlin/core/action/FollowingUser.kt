package core.action

import core.domain.UserRepository
import core.domain.exception.UserDoesNotExistException

class FollowingUser(private val repository: UserRepository) {

    operator fun invoke(actionData: ActionData): Set<String> {
        val user = checkUserExists(actionData)
        return user.followers
    }

    private fun checkUserExists(actionData: ActionData) =
        repository.find(actionData.nickName) ?: throw UserDoesNotExistException()

    data class ActionData(
        val nickName : String
    )
}
