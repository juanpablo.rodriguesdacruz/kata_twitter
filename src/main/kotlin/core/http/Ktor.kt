package core.http

import core.action.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.plugins.defaultheaders.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

class Ktor(private val followingUser: FollowingUser,
           private val registerUser: RegisterUser,
           private val followUser: FollowUser,
           private val updateUser: UpdateUser,
           private val findUser: FindUser,
           private val createTwit: CreateTwit
) {
    fun startServer() {
        embeddedServer(Netty, port = 8080, host = "0.0.0.0") {
            install(DefaultHeaders)
            install(ContentNegotiation) {
                json(
                    Json {
                        ignoreUnknownKeys = true
                        explicitNulls = false
                    }
                )
            }

            routing {
                get("/user/{nickName}/following") {
                    val nickName = this.context.parameters["nickName"]!!
                    val actionData = FollowingUser.ActionData(nickName)
                    val followers = followingUser(actionData)
                    val result = followers.map { it }
                    this.call.respond(result)
                }
                get("/user/{nickName}/find") {
                    val nickName = this.context.parameters["nickName"]!!
                    val actionData = FindUser.ActionData(nickName)
                    val user = findUser(actionData)
                    val result = UserRepresentation.from(user)
                    this.call.respond(result)
                }
                put("/user/{nickName}/{realName}") {
                    val nickName = this.context.parameters["nickName"]!!
                    val realName = this.context.parameters["realName"]!!
                    val actionData = RegisterUser.ActionData(realName, nickName)
                    registerUser(actionData)
                    this.call.respond(HttpStatusCode.Created)
                }
                post("/user/{nickName}/twit") {
                    val nickName = this.context.parameters["nickName"]!!
                    val twitText = this.call.receive<TwitRepresentation>()
                    val actionData = CreateTwit.ActionData(nickName, twitText.twit)
                    createTwit(actionData)
                    this.call.respond(HttpStatusCode.Created)
                }
                post("/user/{follower}/follow/{followed}") {
                    val follower = this.context.parameters["follower"]!!
                    val followed = this.context.parameters["followed"]!!
                    val actionData = FollowUser.ActionData(follower, followed)
                    followUser(actionData)
                    this.call.respond(HttpStatusCode.Created)
                }
                post("/user/{nickName}/{realName}") {
                    val nickName = this.context.parameters["nickName"]!!
                    val realName = this.context.parameters["realName"]!!
                    val actionData = UpdateUser.ActionData(realName, nickName)
                    updateUser(actionData)
                    this.call.respond(HttpStatusCode.Created)
                }
            }
        }.start(true)
    }
}

@Serializable
class FollowerRepresentation(
    val follower: String
)

@Serializable
class TwitRepresentation(
    @SerialName("twit") var twit: String
)