package core.http

import core.domain.User
import kotlinx.serialization.Serializable

@Serializable
class UserRepresentation(
    val nickName: String,
    val realName: String,
    val followers: List<FollowerRepresentation> = listOf(),
    val twits: List<TwitRepresentation> = listOf()
) {
    companion object {
        fun from(user: User) = with(user) {
            UserRepresentation(
                nickName,
                realName,
                followers.map { FollowerRepresentation(it) },
                twits.map { TwitRepresentation(it) }
            )
        }
    }
}
