package core.action

import core.domain.User
import core.domain.UserRepository
import core.domain.exception.UserDoesNotExistException
import io.mockk.every
import io.mockk.mockk
import org.junit.Before
import org.junit.Test
import kotlin.test.assertContains
import kotlin.test.assertTrue

class CreateTwitShould {
    private lateinit var repository: UserRepository
    private lateinit var createTwit: CreateTwit
    private lateinit var actionData: CreateTwit.ActionData
    private lateinit var currentUser: User
    private var throwable: Throwable? = null

    @Before
    fun setUp() {
        repository = mockk(relaxUnitFun = true)
        createTwit = CreateTwit(repository)
    }

    @Test
    fun `create twit`() {
        givenATwit()
        givenAnUserExists()
        whenCreateATwit()
        thenUserAddANewTwit()
    }

    @Test
    fun `can not create twit when user does not exists`() {
        givenATwit()
        givenAnUserDoesNotExists()
        whenCreateATwit()
        thenAnUserDoesNotExistsExceptionWasThrown()
    }

    private fun givenAnUserDoesNotExists() {
        every { repository.find(NICK_NAME) }.returns(null)
    }

    private fun givenATwit() {
        actionData = CreateTwit.ActionData(NICK_NAME, TWIT)
    }

    private fun givenAnUserExists() {
        every { repository.find(NICK_NAME) }.returns(createUserWithTwit())
    }

    private fun whenCreateATwit() {
        throwable = runCatching {
            currentUser = createTwit(actionData)
        }.exceptionOrNull()
    }

    private fun thenUserAddANewTwit() {
        assertContains(currentUser.twits, TWIT)
    }

    private fun thenAnUserDoesNotExistsExceptionWasThrown() {
        assertTrue { throwable is UserDoesNotExistException }
    }

    private fun createUserWithTwit(): User {
        val user = User("", NICK_NAME)
        user.addTwit(TWIT)
        return user
    }

    private companion object {
        const val NICK_NAME = "@jackBauer"
        const val TWIT = "twit test"
    }
}
