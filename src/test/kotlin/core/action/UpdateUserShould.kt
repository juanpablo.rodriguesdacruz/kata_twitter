package core.action

import core.domain.User
import core.domain.UserService
import io.mockk.mockk
import io.mockk.verify
import org.junit.Before
import org.junit.Test

class UpdateUserShould {

    private lateinit var actionData : UpdateUser.ActionData
    private lateinit var updateUser: UpdateUser
    private lateinit var userService: UserService

    @Before
    fun setUp() {
        userService = mockk(relaxUnitFun = true)
        updateUser = UpdateUser(userService)
    }

    @Test
    fun `update realName`() {
        givenAnUser()
        whenUpdateUser()
        thenTheRealNameWasUpdated()
    }

    private fun givenAnUser() {
        actionData = UpdateUser.ActionData(REAL_NAME, NICK_NAME)
    }

    private fun whenUpdateUser() {
        updateUser(actionData)
    }

    private fun thenTheRealNameWasUpdated() {
        val user = User(actionData.realName, actionData.nickName)
        verify { userService.update(user) }
    }

    private companion object {
        const val REAL_NAME = "Jack Bauer"
        const val NICK_NAME = "@jackBauer"
    }
}