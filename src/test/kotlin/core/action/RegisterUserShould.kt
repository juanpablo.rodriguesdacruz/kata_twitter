package core.action

import core.domain.User
import core.domain.UserService
import io.mockk.mockk
import io.mockk.verify
import org.junit.Before
import org.junit.Test

class RegisterUserShould {
    private lateinit var actionData : RegisterUser.ActionData
    private lateinit var userService: UserService
    private lateinit var registerUser : RegisterUser

    @Before
    fun setUp() {
        userService = mockk(relaxUnitFun = true)
        registerUser = RegisterUser(userService)
    }

    @Test
    fun `can register user`() {
        givenAnUser()
        whenRegisterUser()
        thenUserWasRegistered()
    }

    private fun givenAnUser() {
        actionData = RegisterUser.ActionData(REAL_NAME, NICK_NAME)
    }

    private fun whenRegisterUser() {
        registerUser(actionData)
    }

    private fun thenUserWasRegistered() {
        val user = User(actionData.realName, actionData.nickName)
        verify { userService.create(user) }
    }

    private companion object {
        const val REAL_NAME = "Jack Bauer"
        const val NICK_NAME = "@jackBauer"
    }
}
