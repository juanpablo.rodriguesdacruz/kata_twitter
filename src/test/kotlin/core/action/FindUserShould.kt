package core.action

import core.domain.User
import core.domain.UserRepository
import core.domain.exception.UserDoesNotExistException
import io.mockk.every
import io.mockk.mockk
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class FindUserShould {
    private lateinit var findUser: FindUser
    private lateinit var userRepository: UserRepository
    private var throwable: Throwable? = null
    private lateinit var actionData: FindUser.ActionData
    private lateinit var user: User

    @Before
    fun setUp() {
        userRepository = mockk()
        findUser = FindUser(userRepository)
        actionData = FindUser.ActionData(NICK_NAME)
    }

    @Test
    fun `can find user`() {
        givenAnUser()
        whenFindAnUser()
        thenReturnAnExistingUser()
    }

    @Test
    fun `can not find an user`() {
        givenANullUser()
        whenFindAnNullUSer()
        thenUserDoesNotExistExceptionWasThrown()
    }

    private fun givenAnUser() {
        every { userRepository.find(actionData.nickName) }.returns(USER)
    }

    private fun givenANullUser() {
        every { userRepository.find(NICK_NAME) }.returns(null)
    }

    private fun whenFindAnNullUSer() {
        throwable = runCatching {
            findUser(actionData)
        }.exceptionOrNull()
    }

    private fun whenFindAnUser() {
        user = findUser(actionData)
    }

    private fun thenReturnAnExistingUser() {
        assertEquals(NICK_NAME, user.nickName)
    }

    private fun thenUserDoesNotExistExceptionWasThrown() {
        assertTrue { throwable is UserDoesNotExistException }
    }

    private companion object {
        const val REAL_NAME = "Jack Bauer"
        const val NICK_NAME = "@jackBauer"
        val USER = User(REAL_NAME, NICK_NAME)
    }
}
