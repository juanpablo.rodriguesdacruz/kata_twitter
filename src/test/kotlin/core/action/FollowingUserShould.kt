package core.action

import core.domain.User
import core.domain.UserRepository
import core.domain.exception.UserDoesNotExistException
import io.mockk.every
import io.mockk.mockk
import org.junit.Before
import org.junit.Test
import kotlin.test.assertContains
import kotlin.test.assertTrue

class FollowingUserShould {
    private lateinit var userFollowers: Set<String>
    private lateinit var followingUser: FollowingUser
    private lateinit var userRepository: UserRepository
    private var throwable: Throwable? = null

    @Before
    fun setUp() {
        userRepository = mockk()
        followingUser = FollowingUser(userRepository)
    }

    @Test
    fun `return followers of a user`() {
        givenAUserWithFollowers()

        whenFollowingUser()

        thenFollowersAreReturned()
    }

    @Test
    fun `return empty followers when user has none`() {
        giverAUserWithoutFollowers()

        whenFollowingUser()

        thenUserHasNotFollowers()
    }

    @Test
    fun `throw exception when user does not exist`() {
        givenANullUser()

        whenFollowingUser()

        thenUserWasNotCreate()
    }

    private fun givenANullUser() {
        every { userRepository.find(NICK_NAME) } returns null
    }

    private fun giverAUserWithoutFollowers() {
        every { userRepository.find(NICK_NAME) } returns USER_WITHOUT_FOLLOWERS
    }

    private fun givenAUserWithFollowers() {
        every { userRepository.find(NICK_NAME) } returns createUserWithFollower()
    }

    private fun whenFollowingUser() {
        throwable = runCatching {
            val actionData = FollowingUser.ActionData(NICK_NAME)
            userFollowers = followingUser(actionData)
        }.exceptionOrNull()
    }

    private fun thenFollowersAreReturned() {
        assertContains(userFollowers, FOLLOWED)
    }

    private fun thenUserWasNotCreate() {
        assertTrue { throwable is UserDoesNotExistException }
    }

    private fun thenUserHasNotFollowers() {
        assertTrue { userFollowers.isEmpty() }
    }

    private fun createUserWithFollower(): User {
        val user = User("", NICK_NAME)
        user.follow(FOLLOWED)
        return user
    }

    private companion object {
        const val NICK_NAME = "@jackBauer"
        const val FOLLOWED = "@amc"
        val USER_WITHOUT_FOLLOWERS = User("", NICK_NAME)
    }
}