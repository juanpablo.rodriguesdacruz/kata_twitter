package core.action

import core.domain.User
import core.domain.UserRepository
import core.domain.exception.UserDoesNotExistException
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.Before
import org.junit.Test
import kotlin.test.assertContains
import kotlin.test.assertTrue

class FollowUserShould {

    private lateinit var currentUser: User
    private lateinit var userRepository: UserRepository
    private lateinit var actionData: FollowUser.ActionData
    private lateinit var followUser: FollowUser
    private var throwable: Throwable? = null

    @Before
    fun setUp() {
        userRepository = mockk(relaxUnitFun = true)
        followUser = FollowUser(userRepository)
    }

    @Test
    fun `user can follow another user`() {
        givenAnUserFollowingAnotherUser()
        givenAFollowerWithoutExistingFollowed()
        givenAFollowed()

        whenFollowUser()

        thenUserFollowAnotherUser()
    }

    @Test
    fun `user can follow a second user`() {
        givenAnUserFollowingAnotherUser()
        givenAFollower()
        givenAFollowed()

        whenFollowUser()

        thenUserCanFollowTwoUsers()
    }

    @Test
    fun `save the user`() {
        givenAnUserFollowingAnotherUser()
        givenAFollower()
        givenAFollowed()

        whenFollowUser()

        thenUserWasSaved()
    }

    @Test
    fun `throw exception when follower does not exist`() {
        givenAnUserFollowingAnotherUser()
        givenAFollowed()
        givenAFollowerNull()

        whenFollowUser()

        thenNickNameDoesNotExistExceptionWasThrown()
    }

    @Test
    fun `throw exception when followed does not exist`() {
        givenAnUserFollowingAnotherUser()
        givenANullFollowed()

        whenFollowUser()

        thenNickNameDoesNotExistExceptionWasThrown()
    }

    private fun givenANullFollowed() {
        every { userRepository.find(FOLLOWED) } returns null
    }

    private fun givenAFollowerWithoutExistingFollowed() {
        every { userRepository.find(FOLLOWER) } returns USER
    }

    private fun givenAFollowerNull() {
        every { userRepository.find(FOLLOWER) } returns null
    }

    private fun givenAFollower() {
        every { userRepository.find(FOLLOWER) } returns createUserWithFollower()
    }

    private fun givenAnUserFollowingAnotherUser() {
        actionData = FollowUser.ActionData(FOLLOWER, FOLLOWED)
    }

    private fun givenAFollowed() {
        every { userRepository.find(FOLLOWED) } returns USER_FOLLOWED
    }

    private fun whenFollowUser() {
        throwable = runCatching {
            currentUser = followUser(actionData)
        }.exceptionOrNull()
    }

    private fun thenUserFollowAnotherUser() {
        assertContains(currentUser.followers, FOLLOWED)
    }

    private fun thenNickNameDoesNotExistExceptionWasThrown() {
        assertTrue { throwable is UserDoesNotExistException }
    }

    private fun thenUserWasSaved() {
        verify { userRepository.save(currentUser) }
    }

    private fun thenUserCanFollowTwoUsers() {
        assertContains(currentUser.followers, FOLLOWED)
        assertContains(currentUser.followers, ANOTHER_FOLLOWED)
    }

    private fun createUserWithFollower(): User {
        val user = User("", FOLLOWER)
        user.follow(ANOTHER_FOLLOWED)
        return user
    }

    private companion object {
        const val FOLLOWER = "@jp"
        const val FOLLOWED = "@amc"
        const val ANOTHER_FOLLOWED = "@mc"
        val USER = User("", FOLLOWER)
        val USER_FOLLOWED = User("", FOLLOWED)
    }
}