package core.infrastructure

import core.domain.User
import core.domain.UserRepository
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull

abstract class UserRepositoryShould {
    private lateinit var repository: UserRepository

    @Before
    fun setUp() {
        repository = getRepository()
    }

    abstract fun getRepository(): UserRepository

    @Test
    fun `save user`() {
        whenSaveUser()

        thenUserWasSaved()
    }

    @Test
    fun `save existing user`() {
        giverAnUser()

        whenSaveUser()

        thenUserWasSaved()
    }

    @Test
    fun `update user`() {
        giverAnUser()

        whenUpdateAnUser()

        thenCanFindThem()
    }

    @Test
    fun `do not update user when not exists`() {
        whenUpdateAnUserDoesNotExiste()

        thenCanNotFindThem()
    }

    private fun giverAnUser() {
        repository.save(USER)
    }

    private fun whenUpdateAnUserDoesNotExiste() {
        repository.update(USER)
    }

    private fun whenSaveUser() {
        repository.save(USER)
    }

    private fun whenUpdateAnUser() {
        repository.update(USER_TO_UPDATE)
    }

    private fun thenUserWasSaved() {
        assertEquals(USER, repository.find(USER.nickName))
    }

    private fun thenCanNotFindThem() {
        assertNull(repository.find(USER.nickName))
    }

    private fun thenCanFindThem() {
        assertEquals(USER_TO_UPDATE, repository.find(USER_TO_UPDATE.nickName))
    }

    private companion object {
        const val REAL_NAME = "Jack Bauer"
        const val NICK_NAME = "@jackBauer"
        const val REAL_NAME_UPDATE = "Eddie VH"
        val USER = User(REAL_NAME, NICK_NAME)
        val USER_TO_UPDATE = USER.copy(realName = REAL_NAME_UPDATE)
    }
}

class InMemoryUserRepositoryShould: UserRepositoryShould() {
    override fun getRepository(): UserRepository {
        return InMemoryUserRepository()
    }
}

class FileUserRepositoryShould: UserRepositoryShould() {
    override fun getRepository(): UserRepository {
        return FileUserRepository()
    }
}
