package core.domain

import core.domain.exception.UserExistsException
import core.domain.exception.UserDoesNotExistException
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.Before
import org.junit.Test
import kotlin.test.assertTrue

class UserServiceShould {
    private lateinit var user: User
    private lateinit var userRepository: UserRepository
    private lateinit var userService: UserService
    private var error: Throwable? = null

    @Before
    fun setUp() {
        userRepository = mockk(relaxUnitFun = true)
        userService = UserService(userRepository)
    }

    @Test
    fun `user can register`() {
        givenAnUser()
        giverAnUserDoesNotExists()
        whenCreate()
        thenUserWasRegistered()
    }

    @Test
    fun `do not register player when nickname is not available`() {
        givenAnUser()
        givenAnExistingUser()
        try {
            whenCreate()
        } catch (e: Exception) {
            thenUserWasNotRegistered()
        }
    }

    @Test(expected = UserExistsException::class)
    fun `throw exception when nickname is not available`() {
        givenAnUser()
        givenAnExistingUser()
        whenCreate()
    }

    @Test
    fun `user can update his real name`() {
        givenAnUser()
        givenAnExistingUser()
        whenUpdateUser()
        thenTheRealNameWasUpdated()
    }

    @Test
    fun `not update real name when user does not exist`() {
        givenAnUser()
        giverAnUserDoesNotExists()
        whenUpdateUser()
        thenTheRealNameWasNotUpdated()
    }

    @Test
    fun `throw exception when nickname is not exist`() {
        givenAnUser()
        giverAnUserDoesNotExists()
        whenUpdateUser()
        thenAnUserDoesNotExistsExceptionWasThrown()
    }

    private fun giverAnUserDoesNotExists() {
        every { userRepository.find(NICK_NAME) } returns null
    }

    private fun givenAnExistingUser() {
        every { userRepository.find(NICK_NAME) } returns USER
    }

    private fun givenAnUser() {
        user = User(REAL_NAME, NICK_NAME)
    }

    private fun whenCreate() {
        userService.create(user)
    }

    private fun whenUpdateUser() {
        error = runCatching {
            userService.update(user)
        }.exceptionOrNull()
    }

    private fun thenTheRealNameWasUpdated() {
        verify { userRepository.update(user) }
    }

    private fun thenAnUserDoesNotExistsExceptionWasThrown() {
        assertTrue { error is UserDoesNotExistException }
    }

    private fun thenTheRealNameWasNotUpdated() {
        verify(exactly = 0) { userRepository.update(any()) }
    }

    private fun thenUserWasRegistered() {
        verify { userRepository.save(user) }
    }

    private fun thenUserWasNotRegistered() {
        verify(exactly = 0) { userRepository.save(any()) }
    }

    private companion object {
        const val REAL_NAME = "Jack Bauer"
        const val NICK_NAME = "@jackBauer"
        val USER = User(REAL_NAME, NICK_NAME)
    }
}
